import { AppDataSource } from "./data-source"
import { Type } from "./entity/Type";


AppDataSource.initialize()
    .then(async () => {
        const typesRepository = AppDataSource.getRepository(Type);
        await typesRepository.clear();
        var types = new Type();
        types.id = 1;
        types.name = "drink";
        await typesRepository.save(types);

        var types = new Type();
        types.id = 2;
        types.name = "bekery";
        await typesRepository.save(types);

        var types = new Type();
        types.id = 3;
        types.name = "food";
        await typesRepository.save(types);

        const typess = await typesRepository.find({ order: { id: "asc" } });
        console.log(typess);
    })
    .catch(error => console.log(error));
