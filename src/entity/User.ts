import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, ManyToMany, JoinTable, OneToMany } from "typeorm"
import { Role } from "./Role";
import { Order } from "./Order";

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    email: string;

    @Column()
    password: string;

    @Column()
    gender: string;

    @CreateDateColumn()
    created: Date;

    @UpdateDateColumn()
    update: Date;

    @ManyToMany(() => Role, (role) => role.users)
    @JoinTable()
    roles: Role[];


    @OneToMany(() => Order, (Order) => Order.user)
    orders: Order[];

}
